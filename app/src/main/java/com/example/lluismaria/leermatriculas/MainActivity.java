package com.example.lluismaria.leermatriculas;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity  {

    private FirebaseDatabase database;
    private DatabaseReference myRefUbicacion;
    private DatabaseReference myRefMatricula;
    private ArrayList<String> llistaMatricules = new ArrayList();

    private ArrayList<Ubicacion>llistaUbicacions = new ArrayList();
    private ValueEventListener childEventListenerMatricula;
    private MyAdapter listAdapter;
    private Spinner spinner;
    private ArrayAdapter<CharSequence> spinnerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinnerAdapter =  new ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, llistaMatricules);
// Specify the layout to use when the list of choices appears
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        obtenerMatriculas();





        }


        public void llenarSpinner()
    {

        spinner = (Spinner) findViewById(R.id.sp_matricula);
// Create an ArrayAdapter using the string array and a default spinner layout

// Apply the spinnerAdapter to the spinner
        spinner.setAdapter(spinnerAdapter);
        spinnerAdapter.notifyDataSetChanged();
        Log.i("llenarSpiner", "llenarSpinner");

       spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {


                Log.i("onItemSelected", "onItemSelected");

                buscarUbicacionPorMatricula(llistaMatricules.get(i));


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.i("onNothingSelected", "onNothingSelected");

            }
        });


    }


        public void obtenerMatriculas()
    {
        database = FirebaseDatabase.getInstance();

        myRefMatricula = database.getReference("ubicacion");
        String test = myRefMatricula.getKey();
        // test val: ubicacion
        this.childEventListenerMatricula = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot matriculaSnapshot: dataSnapshot.getChildren()) {
                    llistaMatricules.add(matriculaSnapshot.getKey());


                }
                llenarSpinner();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        myRefMatricula.addListenerForSingleValueEvent(childEventListenerMatricula);


    }


    private void llenarListView() {

        listAdapter = new MyAdapter(this, llistaUbicacions);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String coordenadas = "geo:"+listAdapter.getItem(position).latitud+", "+listAdapter.getItem(position).longitud;
                Uri gmmIntentUri = Uri.parse(coordenadas);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });
        listAdapter.notifyDataSetChanged();
    }

    private void buscarUbicacionPorMatricula(String s) {
        Log.i("buscarUbicacion", "buscarUbicacionPorMatricula");
        llistaUbicacions.clear();
        if ( listAdapter!= null)
        {listAdapter.notifyDataSetChanged();}

        myRefUbicacion = database.getReference("ubicacion").child(s);
        myRefUbicacion.addChildEventListener( new ChildEventListener( )
    {


        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            Ubicacion last = dataSnapshot.getValue(Ubicacion.class);
            llistaUbicacions.add(0,last);
            Log.i("onChildAdded", "ubicacionEncontrada");
            llenarListView();

        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });



    }


}
